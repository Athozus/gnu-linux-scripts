#!/bin/bash

ip link set dev $1 down
ip link set dev $1 address $2
ip link set dev $1 up