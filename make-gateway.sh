#!/bin/bash
# Based on https://wiki.archlinux.org/title/Internet_sharing

ip link set up dev $2
ip addr add 192.168.123.100/24 dev $2

iptables -t nat -A POSTROUTING -o $1 -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $2 -o $1 -j ACCEPT
