# GNU Linux Scripts

## How to use

Just run the scripts like that `./script.sh`.

## Available programs

### Make Gateway

Name : `make-gateway.sh`

It is used to transfer the connection from a device to another.

Example : connect a device (1) to wi-fi network via ethernet to another device (2). Run on the device 2 : `./make-gateway.sh wlan0 eth0`

### MAC Edit

Name : `mac-edit.sh`

It is used to change your MAC address on a network you're connected.

Example : change your ethernet MAC address to another one (sample here) : `./mac-edit.sh eth0 99:aa:11:bb:22:cc`

## License & Credits

Licensed under terms of GPLv3.0.

Credits : [https://gitlab.com/Athozus Athozus]
